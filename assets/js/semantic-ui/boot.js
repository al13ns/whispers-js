
define( [ 'jquery' , 'assets/semantic-ui/semantic.min.js' ] , function( jQuery , SemanticUI ) {

    let $ = jQuery;

    class SemanticInitializer
    {
        constructor( SemanticUI )
        {
            this.SemanticUI =   SemanticUI;
        }

        initDropdowns()
        {
            $('.ui.dropdown').dropdown({on: 'click'});

            return this;
        }

        initMenus()
        {
            $('.ui.menu a.item').on('click', function() {
                $(this).addClass('active').siblings().removeClass('active');
            });

            return this;
        }

        initProgressBars()
        {
            $('.ui.progress').progress({showActivity: false});

            return this;
        }

        initTabs()
        {
            $('.menu.tabular .item')
                .tab({
                    // history: true,
                    // historyType: 'hash',
                    
                    // history: true,
                    // historyType: 'state' ,
                    // path: '/vue',
                })
            ;

            return this;
        }

        init()
        {
            let self = this;

            $(document).ready(function() {

                self
                    .initDropdowns()
                    .initMenus()
                    .initProgressBars()
                    .initTabs()
                ;

            });
        }
    }

    return new SemanticInitializer( SemanticUI );
});
