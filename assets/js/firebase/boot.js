
define( [ 'firebase/firebase' ] , function( fb ) {

    const config = {
        apiKey: "AIzaSyDGEVx7N6GFNQgZPjcHmg5DbS0Y798WqHE",
        authDomain: "sandbox-a65aa.firebaseapp.com",
        databaseURL: "https://sandbox-a65aa.firebaseio.com",
        projectId: "sandbox-a65aa",
        storageBucket: "sandbox-a65aa.appspot.com",
        messagingSenderId: "309855040670"
    };
    firebase.initializeApp(config);

    return {
        'db'        :   firebase.database().ref() ,
        'sb'        :   firebase.database().ref() ,
        'firebase'  :   firebase
    };
});

