
define( [ 'openpgp' , 'bluebird' ] , function( openpgp , Promise ) {

    class Crypt
    {
        constructor( worker_path )
        {
            this.worker_path    =   worker_path;
        }

        init()
        {
            openpgp.initWorker({ 'path' : this.worker_path });

            return this;
        }

        generateKey( length )
        {
            return this.generateRandomString( length );
        }

        spawn()
        {
            // return new openpgp.AsyncProxy({ 'worker' : openpgp.getWorker() });
            return new openpgp.AsyncProxy({ 'path' : this.worker_path });
        }

        /**
         *
         * @param array|string userid
         * @param string password
         * @param number bits
         * @returns {Promise}
         */
        generateKeyPair( userid , password , bits )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                let pgp =   self.spawn();

                let options =
                {
                    'numBits':      bits || 4096,
                    'userIds':      Array.isArray( userid ) ? userid : [{ 'name' : userid }],
                    'passphrase':   password
                };

                console.log( "PGP" , pgp );

                Promise.try( ()=>
                {
                    return pgp.delegate( 'generateKey' , options );
                })
                .then( function( keypair )
                {
                    // success
                    let privkey     = keypair.privateKeyArmored;
                    let pubkey      = keypair.publicKeyArmored;

                    console.log( keypair );
                    console.log( privkey );
                    console.log( pubkey );

                    resolve( keypair );
                })
                .catch( function( error )
                {
                    console.log( error );
                    reject( error );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });

            });
        }

        importPrivKey( privkey , password )
        {
            let privateKey = this.importKey( privkey )[0];

            let dec =   privateKey.decrypt( password ); //TODO: if no password, decrypts?
            console.log( 'password' , password );
            console.log( 'decrypted PK' , dec );
            if( dec )
            {
                return privateKey;
            }
            else
            {
                throw new Error( "private key decryption failed. wrong password?" );
            }
        }

        importPubKey( pubkey )
        {
            let publicKey   =    this.importKey( pubkey )[0];
            return publicKey;
        }

        importKey( key )
        {
            //console.log( 'key' , openpgp.key.readArmored( key ).keys );
            return openpgp.key.readArmored( key ).keys;
        }

        importPrivAndPubKey( _pubkey , _privkey , password )
        {
            let pubkey  =   this.importPubKey( _pubkey );
            let privkey =   this.importPrivKey( _privkey , password );

            pubkey.update( privkey );

            //TODO: if pub and private key do not match
            //if( user.pubKey.getUserIds() !== user.privKey.getUserIds() )

            //pubkey is now private
            if( !pubkey.isPrivate() )
            {
                throw new Error( "import failed" );
            }

            return pubkey;
        }

        /**
         *
         * @param array<key>|key pubKeys
         * @param string text
         * @returns {Promise}
         */
        encrypt( pubKeys , text )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                //asyncProxy.delegate('encrypt', { data, publicKeys, privateKeys, passwords, filename, armor });
                let pgp =   self.spawn();

                let options =
                {
                    data        :   text ,
                    publicKeys  :   Array.isArray( pubKeys ) ? pubKeys : [ pubKeys ]
                };

                Promise.try(()=>
                {
                    return pgp.delegate( 'encrypt' , options );
                })
                .then( function( ciphertext )
                {
                    // success
                    console.log( 'encrypt.success' , ciphertext );
                    resolve( ciphertext.data );
                })
                .catch(function(error)
                {
                    // failure
                    reject( error );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });
            });
        }

        decrypt( privKey , text )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                //asyncProxy.delegate('decrypt', { message, privateKey, publicKeys, sessionKey, password, format });
                let pgp =   self.spawn();

                let options =
                {
                    message     :   openpgp.message.readArmored( text ) ,
                    privateKey  :   privKey
                };

                Promise.try(()=>
                {
                    return pgp.delegate( 'decrypt' , options );
                })
                .then( function( plaintext )
                {
                    // success
                    console.log( 'decrypt.success' , plaintext );
                    resolve( plaintext.data );
                })
                .catch( function( error )
                {
                    // failure
                    reject( error );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });
            });
        }

        /**
         *
         * @param array<key>|key privKeys
         * @param text
         * @returns {Promise}
         */
        sign( privKeys , text )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                //asyncProxy.delegate('sign', { data, privateKeys, armor });
                let pgp =   self.spawn();

                let options =
                {
                    data        :   text ,
                    privateKeys :   Array.isArray( privKeys ) ? privKeys : [ privKeys ]
                };

                Promise.try(()=>
                {
                    return pgp.delegate( 'sign' ,  options );
                })
                .then( function( signedtext )
                {
                    // success
                    console.log( 'sign.success' , signedtext );
                    resolve( signedtext.data );
                })
                .catch( function( error )
                {
                    // failure
                    reject( error );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });
            });
        }

        /**
         *
         * @param array<key>|key pubKeys
         * @param string text
         * @returns {Promise}
         */
        verify( pubKeys , text )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                //asyncProxy.delegate('verify', { message, publicKeys });
                let pgp =   self.spawn();

                let options =
                {
                    message     :   openpgp.cleartext.readArmored( text ) ,
                    publicKeys  :   Array.isArray( pubKeys ) ? pubKeys : [ pubKeys ]
                };

                Promise.try(()=>
                {
                    return pgp.delegate( 'verify' , options );
                })
                .then( function( result )
                {
                    console.log( 'verify.success' , result );

                    let valid = false;
                    for( let i in result.signatures )
                    {
                        if( result.signatures[i].valid )
                        {
                            valid = true;
                            break;
                        }
                    }

                    if( valid )
                    {
                        // success
                        resolve( result.data );
                    }
                    else
                    {
                        throw new Error( "validation failed" );
                    }

                })
                .catch( function( error )
                {
                    // failure
                    console.log( "ERR" , error );
                    reject( error );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });
            });
        }

        /**
         *
         * @param array<key>|key pubKeys
         * @param array<key>|key privKey
         * @param string text
         * @returns {Promise}
         */
        signAndEncrypt( pubKeys , privKey , text )
        {
            let self = this;

            return self.sign( privKey , text )
            .then( function( signedText )
            {
                return self.encrypt( pubKeys , signedText );
            });

            /*
            return new Promise(
            function( resolve , reject )
            {
                //asyncProxy.delegate('encrypt', { data, publicKeys, privateKeys, passwords, filename, armor });
                let pgp =   self.spawn();

                let options =
                {
                    data        :   text ,
                    publicKeys  :   Array.isArray( pubKeys ) ? pubKeys : [ pubKeys ]
                    privateKeys :   Array.isArray( privKey ) ? privKey : [ privKey ]
                };

                pgp.delegate( 'encrypt' , options )
                .then( function( ciphertext )
                {
                    // success
                    console.log( 'sign&encrypt.success' , ciphertext );
                    resolve( ciphertext.data );
                })
                .catch(function(error)
                {
                    // failure
                    reject( error );
                });
            });
            */
        }

        decryptAndVerify( privKey , pubKeys , text )
        {
            let self = this;

            return self.decrypt( privKey , text )
            .then( function( decryptedText )
            {
                return self.verify( pubKeys , decryptedText );
            });

            /*
            return new Promise(
            function( resolve , reject )
            {
                //asyncProxy.delegate('decrypt', { message, privateKey, publicKeys, sessionKey, password, format });
                let pgp =   self.spawn();

                let options =
                {
                    message     :   openpgp.message.readArmored( text ) ,
                    privateKey  :   privKey ,
                    publicKeys  :   Array.isArray( pubKeys ) ? pubKeys : [ pubKeys ]
                };

                pgp.delegate( 'decrypt' , options )
                .then( function( plaintext )
                {
                    // success
                    console.log( 'decrypt&verify.success' , plaintext );
                    resolve( plaintext.data );
                })
                .catch( function( error )
                {
                    // failure
                    reject( error );
                });
            });
            */
        }

        /**
         *
         * @param array|string password
         * @param string text
         * @returns {Promise}
         */
        symEncrypt( password , text )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                let pgp = self.spawn();

                let options =
                {
                    data        :   text.toString() ,
                    passwords   :   Array.isArray( password ) ? password : [ password.toString() ]
                };

                Promise.try(()=>
                {
                    return pgp.delegate( 'encrypt' , options );
                })
                .then(function(ciphertext)
                {
                    resolve( ciphertext.data );
                })
                .catch( function( err )
                {
                    reject( err );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });
            });
        }

        /**
         *
         * @param string password
         * @param string text
         * @returns {Promise}
         */
        symDecrypt( password , text )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                let pgp = self.spawn();

                let options =
                {
                    message     :   openpgp.message.readArmored( text.toString() ),
                    password    :   password.toString()
                };

                Promise.try(()=>
                {
                    return pgp.delegate( 'decrypt' , options );

                })
                .then(function(plaintext)
                {
                    resolve( plaintext.data );
                })
                .catch( function( err )
                {
                    reject( err );
                })
                .finally(function()
                {
                    pgp.terminate();
                    pgp = undefined;
                });
            });
        }

        getUserIdFromKey( key )
        {
            if( !key instanceof openpgp.key.Key )
            {
                throw new Error( "not a openpgp.key.Key instance" );
            }

            // console.log(key.getUserIds());
            // console.log(key.getPrimaryUser().user);

            //return key.key.getUserIds()[0];
            let uid = key.getPrimaryUser().user.userId.userid;

            if( uid.indexOf( '<>' ) !== -1 )
            {
                uid = uid.replace( '<>' , '' );
            }

            return uid.trim();
        }

        /**** utils ****/

        generateRandomString( length = 32 )
        {
            let text = "";
            let validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            let randoms =   this.generateRandomNumbers( length );

            for(let i = 0; i < length; i++) {
                text += validChars.charAt( Math.floor( randoms[i] / (0xffffffff + 1) * validChars.length ) );
                // text += validChars.charAt( this.generateRandomNumber( 0 , validChars.length - 1 ) );
                // text += validChars.charAt(Math.floor(Math.random() * validChars.length));
            }

            return text;
        }

        hash2(str, asString, seed)
        {
            /*jshint bitwise:false */
            let i, l,
                hval = (seed === undefined) ? 0x811c9dc5 : seed;

            for (i = 0, l = str.length; i < l; i++) {
                hval ^= str.charCodeAt(i);
                hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
            }
            if( asString ){
                // Convert to 8 digit hex string
                return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
            }
            return hval >>> 0;
        }

        hash1( string , asString )
        {
            let hash = 0, i, chr, len;
            if (string.length === 0) return hash;
            for (i = 0, len = string.length; i < len; i++) {
                chr   = string.charCodeAt(i);
                hash  = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }

            if(asString)
            {
                return ("0000000" + (hash >>> 0).toString(16)).substr(-8);
            }

            return hash;
        }

        generateRandomNumber( min = 0 , max = Number.MAX_SAFE_INTEGER )
        {
            const crypto  =   window.crypto || window.msCrypto; // for IE 11
            let randomNumber;
            min = Math.ceil(min);
            max = Math.floor(max);

            if( crypto )
            {
                const randomBuffer = new Uint32Array(1);

                crypto.getRandomValues(randomBuffer);

                randomNumber = randomBuffer[0] / (0xffffffff + 1);
            }
            else
            {
                randomNumber    =   Math.random();
            }

            return Math.floor(randomNumber * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
        }

        generateRandomNumbers( howMany = 10 )
        {
            let crypto  =   window.crypto || window.msCrypto; // for IE 11
            let array   =   new Uint32Array(howMany);

            let getRandomIntInclusive = function( min = null , max = null )
            {
                min     =   min ? Math.ceil(min) : Number.MIN_SAFE_INTEGER;
                max     =   max ? Math.floor(max) : Number.MAX_SAFE_INTEGER;

                return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
            };

            if( crypto )
            {
                crypto.getRandomValues(array);
            }
            else
            {
                //use Math.random()
                for( let i in array )
                {
                    array[i]    =   getRandomIntInclusive( 1 , Number.MAX_SAFE_INTEGER );
                }
            }

            return array;
        }
    }

    return Crypt;
});
