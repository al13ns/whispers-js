
define( [] , function() {

    Set.prototype.toArray   =   function ()
    {
        return Array.from( this );
    };

    Map.prototype.toArray   =   function ()
    {
        return Array.from( this );
    };

    Map.prototype.toJSON   =   function ()
    {
        let json    =   {};

        for( let [ key , val ] of this )
        {
            json[key]   =   val;
        }

        return json;
    };

    Map.prototype.reverse   =   function ()
    {
        let reversed    =   new Map();
        let map         =   new Map( this.toArray().reverse() );

        for( let [ key , val ] of map )
        {
            reversed.set( key , val );
        }

        return reversed;
    };

    Map.fromObject  =   function( object )
    {
        if( null == object )
        {
            return new Map();
        }

        if( object instanceof Map )
        {
            return new Map( Object.entries( object ) );
        }

        if( !object instanceof Object )
        {
            return new Map( object );
        }

        let map     =   new Map();
        let data    =   object;

        for( let key in data )
        {
            map.set( key , data[key] );
        }

        return map;
    };

    Date.fromUTCSeconds    =   function( utcSeconds )
    {
        let date = new Date(0); // The 0 there is the key, which sets the date to the epoch
        date.setUTCSeconds( utcSeconds );

        return date;
    };

    const utils  =   {

        $log : function( data )
        {
            console.log( data );
        },

        $reverseObject : function( data )
        {
            let map     =   new Map();

            for( let key in data )
            {
                map.set( key , data[key] );
            }

            return this.$reverseMap( map );
        },

        $reverseMap : function( data )
        {
            let reversed    =   new Map();
            let map         =   new Map( Array.from( data ).reverse() );

            for( let [ key , val ] of map )
            {
                reversed.set( key , val );
            }

            return reversed;
        },

        $createMap : function( object )
        {
            return Map.fromObject( object );
        },

        $length : function( arrayOrObject )
        {
            if( arrayOrObject instanceof Array )
                return arrayOrObject.length;
            else if( arrayOrObject instanceof Object )
                return Object.keys( arrayOrObject ).length;
            else
                return 0;
        },

        $addArrayItemKeys : function( array )
        {
            var tmp = array;
            for( var key in tmp )
            {
                tmp[key]['.key'] =   key;
            }

            return tmp;
        },

        $sortObjectByKeys : function(obj)
        {
            let keys = Object.keys(obj).sort(function keyOrder(k1, k2) {
                if (k1 < k2) return -1;
                else if (k1 > k2) return +1;
                else return 0;
            });

            let i, after = {};
            for (i = 0; i < keys.length; i++) {
                after[keys[i]] = obj[keys[i]];
                delete obj[keys[i]];
            }

            for (i = 0; i < keys.length; i++) {
                obj[keys[i]] = after[keys[i]];
            }
            return obj;
        },

        $sortObject :   function ( obj , sort = 'number' )
        {
            var arr = [];
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    arr.push({
                        'key': prop,
                        'value': obj[prop]
                    });
                }
            }

            if( 'number' === sort )
            {
                arr.sort(function(a, b) { return a.value - b.value; });
            }
            else if( 'string' === sort )
            {
                arr.sort(function(a, b) { a.value.toLowerCase().localeCompare(b.value.toLowerCase()); }); //use this to sort as strings
            }
            else
            {
                arr.sort();
            }

            // let list = sortObject({"you": 100, "me": 75, "foo": 116, "bar": 15});

            return arr; // returns array
        },

        $nl2br : function( string_with_nl )
        {
            return string_with_nl.replace(/(?:\r\n|\r|\n)/g, '<br />');
        },

        $replaceUrls : function( text , replace )
        {
            replace =   replace || '<a href="$&">$3.$5.$6</a>';

            let regex    =   new RegExp( /(https?:\/\/)((www)(\.))?([-a-zA-Z0-9@:%._\+~#=]{2,256})\.([a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*))/gim );

            if( !regex.test( text ) )
            {
                console.log( 'no url found' );
                return text;
            }

            console.log( 'urls replaced' );
            return text.replace( regex , replace );

            let expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
            regex = new RegExp(expression);
            let t = 'www.google.com';
            if (t.match(regex) )
            {
                alert("Successful match");
            } else {
                alert("No match");
            }
        },

        $loadFile : function( file , mode ) //mode - text, binary, dataurl
        {
            return new Promise(
                function( resolve , reject )
                {
                    let reader  = new   FileReader();
                    reader.onload = function(event)
                    {
                        // The file's text will be printed here
                        console.log (event.target.result );
                        resolve( event.target.result );
                    };

                    if( 'text' === mode )
                    {
                        reader.readAsText( file );
//        reader.readAsText( file , 'CP1250' );
                    }
                    else if( 'binary' === mode )
                    {
                        reader.readAsBinaryString( file );
                    }
                    else if( 'dataurl' === mode )
                    {
                        reader.readAsDataURL( file );
                    }
                    else
                    {
                        reject( 'wrong mode - text|binary|dataurl' );
                    }
                });
        },

        $loadTextFile : function( file )
        {
            return this.$loadFile( file , 'text' );
        }
    };

    return utils;
});


