
define( [ 'app/firebase' , 'openpgp' , 'bluebird' ] , function( fb , openpgp , Promise ) {

    class Whispers
    {
        constructor()
        {}

        get db()
        {
            return fb.firebase.database().ref('whispers');
        }

        userExists( userID )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                self.db.child( 'users' ).child( userID ).once( 'value' , function( user )
                {
                    if( !user.val() )
                    {
                        reject( userID );
                    }
                    else
                    {
                        resolve( userID );
                    }
                });
            });
        }

        userEquals( user , crypt )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                self.db.child( 'users' ).child( user.userID ).once( 'value' , function( snap )
                {
                    let dbUser  =   snap.val();

                    if( !dbUser )
                    {
                        return reject( user.userID );
                    }

                    try
                    {
                        let key = crypt.importPrivAndPubKey( dbUser.pubKey , user.privKey , user.password );

                        console.log( 'key pair verification succeeded' , "success" );
                        console.log( 'user equals dbuser' , "success" );

                        return resolve( user );
                    }
                    catch( err )
                    {
                        console.log( 'key pair verification failed' , "danger" );
                        console.log( err , 'danger' );
                        // throw err;
                        reject(err);
                    }
                });
            });
        }

        removeUser( userID )
        {
            return this.db.child( 'users' ).child( userID ).remove();
        }

        createUser( user , store_priv_key = false )
        {
            let self    =   this;

            return new Promise(
            function( resolve , reject )
            {
                self.userExists( user.userID ).then(
                function()
                {
                    reject( "user with id " + user.userID + " already exists" );
                },
                function()
                {
                    let store   =
                    {
                        "pubKey"    :   user.pubKey instanceof openpgp.key.Key ? user.pubKey.armor() : user.pubKey ,
                        "created"   :   fb.firebase.database.ServerValue.TIMESTAMP
                    };

                    if( store_priv_key )
                    {
                        store.privKey   =   user.privKey instanceof openpgp.key.Key ? user.privKey.armor() : user.privKey ;
                    }

                    self.db.child( "users" ).child( user.userID ).set( store ).then(
                    function()
                    {
                        resolve(user);
                    },
                    function(err)
                    {
                        reject(err);
                    } );
                });
            });
        }

        userLogin( auth , crypt )
        {
            //this.db.child( 'users' ).orderByChild( "password" ).equalTo(25).on("child_added", function(snapshot)

            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                //fetch public key and if available private key

                let user    =   new Whispers.User( auth.userID , auth.password );

                self.db.child( 'users' ).child( auth.userID ).once( 'value' ,
                function( data )
                {
                    if( !data.val() )
                    {
                        return reject( "user " + auth.userID + " not found" );
                    }

                    try
                    {
                        //TODO: reload/verify user - before sending message

                        user.pubKey     =   crypt.importPubKey( data.val().pubKey );
                        user.privKey    =   crypt.importPrivAndPubKey( data.val().pubKey ,  data.val().privKey ? data.val().privKey : auth.privKey , auth.password );

                        console.log( "logged in" );
                        resolve( user );
                    }
                    catch( err )
                    {
                        console.log( "error logging in" );
                        reject( err );
                    }
                });
            });
        }

        subscribeChannels( listener )
        {
            let self = this;

            self.db.child( 'channels' )
            .orderByChild( 'created' )
            .limitToLast( 1000 )
            .on( 'value' , ( snap ) =>
            {
                listener( snap.val() );
            } );
        }

        fetchChannels()
        {
            let self = this;

            return new Promise( function( resolve , reject )
            {
                self.db.child( 'channels' )
                .orderByChild( 'created' )
                .limitToLast( 1000 )
                .once( 'value' , ( snap ) =>
                {
                    console.log( snap.val() );
                    resolve( snap.val() );
                } );
            });
        }

        leaveChannel( { userID } , channel )
        {
            const self = this;

            return new Promise(
            function( resolve , reject )
            {
                let remove  =   {};
                remove["channels/" + channel +"/users/" + userID]    =   null; //setting to null removes it
                remove["users/" + userID +"/channels/" + channel]    =   null;

                console.log( remove );

                self.db.update( remove ).then(
                function()
                {
                    resolve( channel );
                },
                function( err )
                {
                    reject( "did not leave: " + err );
                });
                //TODO: delete channel if empty
            })
            .then(( ch ) =>
            {
                console.log( 'ch' , channel );

                // self.db.child( 'channels' ).child( channel ).child( 'users' ).off( "value");
                // self.db.child( 'channel_messages' ).child( channel ).off( "child_added" );

                self.db
                    .child( 'messages' )
                    .child( channel )
                    .off();

                return channel;
            });
        }

        joinChannel( listener , { userID } , channel )
        {
            let self = this;

            return new Promise(( resolve , reject ) =>
            {
                Promise.try(() =>
                {
                    self.db.child( 'channels' ).child( channel ).once( "value" ,
                    function( channel_data )
                    {
                        if( !channel_data.val() )
                        {
                            throw new Error( "channel not found" );
                        }

                        self.db
                        .child( 'users' )
                        .child( userID )
                        .child( 'channels' )
                        .child( channel )
                        .once( "value" ,
                        function( member_since )
                        {
                            if( !member_since.val() )
                            {
                                //add member
                                let store   =   {};
                                store["channels/" + channel +"/users/" + userID]    =   firebase.database.ServerValue.TIMESTAMP;
                                store["users/" + userID +"/channels/" + channel]    =   firebase.database.ServerValue.TIMESTAMP;

                                console.log( store );

                                self.db.update( store ).then(
                                function()
                                {
                                    console.log( "successfuly joined" );
                                    resolve( channel_data.val() );
                                },
                                function( err )
                                {
                                    reject( err );
                                });

                            }
                            else
                            {
                                console.log( "already a member" );
                                resolve( channel_data.val() );
                            }
                        },
                        function( err )
                        {
                            reject( err );
                        });
                    },
                    function( err )
                    {
                        throw err;
                    });
                })
                .catch(( err ) =>
                {
                    reject( err );
                });
            })
            .then(( ch ) =>
            {
                console.log( 'ch' , channel );

                self.db
                .child( 'messages' )
                .child( channel )
                .on( 'child_added' , (snap) =>
                {
                      // message , messageID , channel
                    listener( snap.val() , snap.key , channel );
                });

                return channel;
            });
        }

        createChannel( channel )
        {
            const self = this;

            return new Promise(
            function( resolve , reject )
            {
                self.db.child( "channels" ).child( channel ).once( "value" ,
                function( ch )
                {
                    if( ch.val() )
                    {
                        reject( new Error( "channel already exists" ) );
                        return;
                    }

                    let store   =   {};

                    store["channels/" + channel +"/created"]                    =   firebase.database.ServerValue.TIMESTAMP;
                    //store["channels/" + channel +"/owner"]                      =   self.getUser().userID;
                    //store["channels/" + channel +"/password"]   =   password;

                    console.log( store );

                    self.db.update( store ).then(
                    function()
                    {
                        resolve( channel );
                    },
                    function( err )
                    {
                        reject( err );
                    });
                });
            });
        }

        subscribeMessages( listener , { userID } , crypt )
        {
            let self = this;

            self.db
                .child( 'users' )
                .child( userID )
                .child( 'channels' )
            .once( 'value' , (snap) =>
            {
                let joinedChannels  =   snap.val();

                for( let ch in joinedChannels )
                {
                    self.db
                        .child( 'messages' )
                        .child( ch )
                    .on( 'child_added' , (snap) =>
                    {
                        //decrypt & verify
                        //TODO: cache pubkeys inside whispers? and decrypt here?

                        // message , messageID , channel
                        listener( snap.val() , snap.key , ch );
                    });
                }
            });
        }

        fetchPubKey( userID )
        {
            let self = this;

            return new Promise(
            function( resolve , reject )
            {
                self.db.child( 'users' ).child( userID ).child( 'pubKey' )
                .once( "value" , function( pubkey_data )
                {
                    if( !pubkey_data.val() )
                    {
                        reject( "pubkey not found" );
                        return;
                    }

                    resolve( pubkey_data.val() );
                });
            });
        }

        loadPubKey( userID , crypt )
        {
            let self = this;

            return new Promise( function ( resolve , reject )
            {
                self.fetchPubKey( userID ).then( function( key )
                {
                    resolve( crypt.importPubKey( key ) );
                });
            } );
        }

        encryptMessage( { privKey , pubKey } , pubKeysArr , plaintext , crypt )
        {
            pubKeysArr.push( pubKey );

            return crypt.signAndEncrypt( pubKeysArr , privKey , plaintext );
        }

        decryptMessage( { privKey } , pubKey , ciphertext , crypt )
        {
            return crypt.decryptAndVerify(
                privKey ,
                pubKey ,
                ciphertext
            );
        }

        submitMessage( sender , channel , text , crypt )
        {
            let self = this;

            return new Promise( function( resolve , reject )
            {
                self.db
                .child( 'channels' )
                .child( channel )
                .child( 'users' )
                .once( 'value' , function( users )
                {
                    resolve( users.val() );
                });
            })
            .then(function( users )
            {
                return Promise.map( Map.fromObject( users ) , function( user )
                {
                    return self.loadPubKey( user[0] , crypt );
                },
                { concurrency : 2 } );
            })
            .then(function( recipientKeys )
            {
                return self.encryptMessage( sender , recipientKeys , text , crypt );
            })
            .then(function( ciphertext )
            {
                let mID   =   self.db.child( 'messages' ).child( channel ).push(
                {
                    "from"  :   sender.toString() ,
                    "sent"  :   firebase.database.ServerValue.TIMESTAMP ,
                    "text"  :   ciphertext
                }).key;

                return ciphertext;
            })
            .catch(function(error)
            {
                console.log(error);
            });
        }

        _someFunc()
        {
            let self = this;

            return new Promise(
                function( resolve , reject )
                {

                });
        }
    }

    Whispers.User   =   class
    {
        constructor( userID , password , pubKey , privKey )
        {
            this.userID     =   userID;
            this.password   =   password;
            this.pubKey     =   pubKey;
            this.privKey    =   privKey;
        }

        toString()
        {
            return this.userID;
        }
    };

    return Whispers;
});
