

define( [ 'loader/fragment' , 'jquery' ] , function( FragmentLoader , $ ) {

    class TemplateLoader extends FragmentLoader
    {
        constructor( tplUri )
        {
            super();

            this.tplUri = tplUri || 'tpl/';
        }

        loadTemplate( ele )
        {
            return this.loadFragment( ele , this.getTplUri( ele ) );
        }

        getTplUri( ele )
        {
            let uri    =   $(ele).data( 'html-src' ) || this.tplUri + $(ele).data('name') + '.html';

            return uri;
        }
    }

    return TemplateLoader;
});
