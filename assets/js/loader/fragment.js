


define( [ 'jquery' ] , function( $ ) {

    class FragmentLoader
    {
        loadFragment( ele , uri )
        {
            return new Promise(
            function( resolve , reject )
            {
                try
                {
                    $(ele).load( uri , function()
                    {
                        resolve();
                    });
                }
                catch(err)
                {
                    reject(err);
                }
            });
        }

        loadScriptWithJQuery( uri )
        {
            return new Promise(
            function( resolve , reject )
            {
                try
                {
                    $.getScript( uri , function( script )
                    {
                        resolve( script );
                    });
                }
                catch(err)
                {
                    reject(err);
                }
            });
        }

        loadScriptWithRequire( uri )
        {
            return new Promise(
            function( resolve , reject )
            {
                try
                {
                    require([ uri ] , function( script )
                    {
                        resolve( script );
                    });
                }
                catch(err)
                {
                    reject(err);
                }
            });
        }

        loadScript( uri )
        {
            return this.loadScriptWithRequire( uri );
        }
    }

    return FragmentLoader;
});
