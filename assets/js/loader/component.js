

define( [ 'loader/template' , 'vue/vue' , 'jquery' ] , function( TemplateLoader , Vue , $ ) {

    class ComponentLoader extends TemplateLoader
    {
        constructor( tplUri , jsUri )
        {
            super( tplUri );

            this.jsUri  =   jsUri || 'tpl/';
        }

        loadComponent( ele , async = false )
        {
            let jsSrc   =   this.getJsUri( ele );
            let self    =   this;

            if( !$(ele).data('name') || !$(ele).attr( 'id' ) )
                return Promise.reject( 'name and id attributes must be set on' + ele );

            return new Promise(
            function( resolve , reject )
            {
                self.loadTemplate( ele )
                .then( function()
                {
                    return self.loadScript( jsSrc );
                })
                .then( function( cmp )
                {
                    if( async )
                    {
                        Vue.component( $(ele).data('name') ,
                            function( resolve , reject )
                            {
                                resolve(cmp.extend({
                                    template: ele
                                }));
                            });
                    }
                    else
                    {
                        Vue.component( $(ele).data('name') ,
                            cmp.extend({
                                template: ele
                            })
                        );
                    }
                })
                .then( function()
                {
                    resolve();
                });
            });
        }

        /**
         *
         * if eles is a string, loads all script[type='x/template'] in 'eles' container id
         * if eles is an array, loads all items
         *
         * @param array|string eles
         * @returns {Promise.<*[]>}
         */
        loadComponents( eles )
        {
            if( !( eles instanceof Array ) )
            {
                let cntnr   =   eles;
                eles        =   [];

                $(cntnr).find( "script[type='x/template']" ).each(function()
                {
                    if( !$(this).data('name') || !$(this).attr( 'id' ) )
                        return;

                    eles.push( '#' + $(this).attr( 'id' ) );
                });
            }

            let promises = [];

            for( let i in eles )
            {
                promises.push( this.loadComponent( eles[i] ) );
            }

            return Promise.all( promises );
        }

        mountComponent( ele , mountEle )
        {
            let self    =   this;

            return new Promise(
            function( resolve , reject )
            {
                self.loadComponent( ele ).then(
                function()
                {
                    try
                    {
                        const $mount = mountEle ? $(mountEle) : $(ele).parent();

                        if( !$mount.attr('id') )
                            throw new Error();

                        resolve(
                            new Vue({
                                el: '#' + $mount.attr('id')
                            })
                        );
                    }
                    catch(err)
                    {
                        reject( "mount element must have an id attribute" );
                    }
                });
            });
        }

        mountComponents( eles )
        {
            let promises = [];

            for( let i in eles )
            {
                promises.push( this.mountComponent( eles[i] ) );
            }

            return Promise.all( promises );
        }

        getJsUri( ele )
        {
            let uri    =   $(ele).data( 'js-src' ) || this.jsUri + $(ele).data('name') + '.js';

            return uri;
        }
    }

    return ComponentLoader;
});
