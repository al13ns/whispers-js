

define( [ 'vue/vue' , 'moment/moment-with-locales' ] , function( Vue , moment ) {

    return {
        methods: {
            $moment :   function( dt )
            {
                return moment( dt || new Date() );
            },

            $momentFormatted   :    function( dt , format )
            {
                return this.$moment( dt ).format( format || 'LLL' );
            },

            $momentFromNow  :   function( dt )
            {
                return this.$moment( dt ).fromNow();
            },

            $momentUtime    :   function( dt )
            {
                return this.$moment( dt ).format( 'X' );
            },

            $momentSetLocale    :   function( locale )
            {
                moment.locale( locale );
            },
        }
    };

});
