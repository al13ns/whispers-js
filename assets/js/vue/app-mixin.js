
define( [ 'vue/vue' ] , function( Vue ) {

    return Vue.extend({
        methods : {
            isCurrentRoute : function( route )
            {
                return this.$route.name === route;
            },
            forceLogin : function()
            {
                if( !this.loggedUser() )
                    this.$route.router.push({ name : 'login' });
            },
            loggedUser : function()
            {
                return firebase.auth().currentUser || null;
            },
            signOut : function()
            {
                var self = this;

                firebase.auth().signOut().then(function()
                {
                    // Sign-out successful.
                    self.$log( 'successfully signed out' , 'warning' );

                    self.$route.router.push({ name : 'login' });

                }, function(error) {
                    // An error happened.
                });
            },
            signIn  :   function()
            {
                this.$route.router.push({ name : 'login' });
            },
            signUp  :   function()
            {
                this.$route.router.push({ name : 'register' });
            },
            goProfile : function()
            {
                this.$route.router.push({ name : 'profile' });
            },
        },
        data : function()
        {
            return {
                'firebase' :    firebase ,
                loggedUserName : null ,
                maxFileSize :   5 //MB
            };
        },
        beforeCreate : function()
        {
            let self = this;

            self.$router.beforeEach((to, from, next) =>
            {
                if (to.matched.some(record => record.meta.requiresAuth))
                {
                    // this route requires auth, check if logged in
                    // if not, redirect to login page.
                    // if (!auth.loggedIn())
                    if ( !self.user )
                    {
                        next({
                            path: '/login',
                            query: { redirect: to.fullPath }
                        })
                    }
                    else
                    {
                        next()
                    }
                }
                else {
                    next() // make sure to always call next()!
                }
            });

            return;

            //https://router.vuejs.org/en/advanced/meta.html


            firebase.auth().onAuthStateChanged( function(user)
            {
                if (user)
                {
                    self.$log( 'user loaded' );
                    self.loggedUserName = user.displayName;

                    //router.go({ name : 'lobby' });
                }
                else
                {
                    self.$log( 'user flushed' );
                    self.loggedUserName = null;

                    //self.$route.router.go({ name : 'login' });
                }
            });

            self.$router.beforeEach(function(to,from,next)
            {
                if( to.auth && !firebase.auth().currentUser
                    && to.name !== 'login'
                    && to.name !== 'register' )
                {
                    next({ 'path' : '/login'});
//            trans.abort('you need to log in');
                }
                else
                {
                    next();
                }
            });
        },
        boot    :   function( router )
        {
            Vue.mountComponent( '#log-view-tpl' )
                .then( function()
                {
                    var cmpnts = [
                        '#login-tpl' , '#profile-tpl' , '#register-tpl' ,
                        '#item-list-tpl' , '#item-tpl' ,
                        '#create-tpl' , '#create-file-tpl' , '#create-text-tpl' , '#create-url-tpl' ,
                        '#create-quick-tpl'
                    ];

                    return Vue.loadComponents( cmpnts );
                })
                .then(function()
                {
                    router.map({
                        '/': {
                            component:  Vue.extend({
                                ready   :   function()
                                {
                                    router.go({ name : 'profile' });
                                }
                            })
                        },

                        '/login': {
                            name: 'login' ,
                            component: Vue.getComponent( 'login' )
                        },

                        '/register': {
                            name: 'register' ,
                            component: Vue.getComponent( 'register' )
                        },

                        '/profile': {
                            name: 'profile' ,
                            component: Vue.getComponent( 'profile' ) ,
                            auth : true
                        },

                        '/shares': {
                            name: 'shares' ,
                            component: Vue.getComponent( 'item-list' ) ,
                            auth : true
                        },

                        '/share/:shareId': {
                            name: 'share' ,
                            component: Vue.getComponent( 'item' ) ,
                            auth : false
                        },

                        '/create': {
                            name: 'create' ,
                            component: Vue.getComponent( 'create' ),
                            auth : true ,
                            subRoutes: {
                                '/file': {
                                    name: 'create-file' ,
                                    component:  Vue.getComponent( 'create-file' )
                                },
                                '/text': {
                                    name: 'create-text' ,
                                    component:  Vue.getComponent( 'create-text' )
                                },
                                '/url': {
                                    name: 'create-url' ,
                                    component:  Vue.getComponent( 'create-url' )
                                },
                                '/quick': {
                                    name: 'create-quick' ,
                                    component:  Vue.getComponent( 'create-quick' )
                                }
                            }
                        }
                    });

                    router.beforeEach(function(trans)
                    {
                        if( trans.to.auth && !firebase.auth().currentUser
                            && trans.to.name !== 'login'
                            && trans.to.name !== 'register' )
                        {
                            trans.redirect('/login');
//            trans.abort('you need to log in');
                        }
                        else
                        {
                            trans.next();
                        }
                    });

                    router.start( Vue.extend( app ) , "#app" );
                });
        }
    });

});
