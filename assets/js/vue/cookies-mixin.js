
define( [ 'vue/vue' , 'js-cookie' ] , function( Vue , Cookies ) {

    return {
        methods: {
            setCookie : function (name, value, expires) {
                Cookies.set(name, value, {expires: expires === undefined ? 7 : expires});
            }
            ,
            getCookie : function (name, defaultValue) {
                return Cookies.getJSON(name) || ( defaultValue === undefined ? null : defaultValue );
            },
            getCookies: function()
            {
                return Cookies.getJSON();
            }
        }
    };

});