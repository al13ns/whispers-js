

define( [ 'loader/component' , 'vue/vue' ] , function( ComponentLoader ,  Vue ) {

    let VueLoader   =
    {
        install :   function( Vue , options )
        {
            let tplUri  =   options && options['htmlUri'] || null;
            let jsUri   =   options && options['jsUri'] || null;

            Vue.loader         =   new ComponentLoader( tplUri , jsUri );
            Vue.loadTemplate   =   function( ele )
            {
                return Vue.loader.loadTemplate( ele );
            };
            Vue.loadComponent   =   function( ele )
            {
                return Vue.loader.loadComponent( ele );
            };
            Vue.mountComponent   =   function( ele , mountEle )
            {
                return Vue.loader.mountComponent( ele , mountEle );
            };
            Vue.loadComponents   =   function( eles )
            {
                return Vue.loader.loadComponents( eles );
            };
            Vue.mountComponents   =   function( eles )
            {
                return Vue.loader.mountComponents( eles );
            };

            Vue.getComponent    =   function( name )
            {
                return Vue.options.components[name];
            }
        }
    };

    return VueLoader;
});
