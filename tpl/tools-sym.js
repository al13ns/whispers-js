

define( [ 'jquery' , 'vue/vue' , 'vue/vue-resource' , 'app/store' ] , function( $ , Vue , VueResource , store ) {

    Vue.use( VueResource );

    return Vue.extend({
        data    :   function()
        {
            return {
                password    :   '' ,
                message     :   '' ,
            };
        },
        methods :   {
            log : function (message)
            {
                this.$parent.$parent.log( message );
            },
            generateKey :   function ( length = 32 )
            {
                this.password = store.getters.crypt.generateKey( length );
            },
            encrypt : function ()
            {
                let self = this;

                store.getters.crypt.symEncrypt( this.password , this.message ).then( function( cipher )
                {
                    self.message    =   cipher;
                })
                .catch(function (e)
                {
                    self.log( e );
                });
            },
            decrypt : function ()
            {
                let self = this;

                store.getters.crypt.symDecrypt( this.password , this.message ).then( function( text )
                {
                    self.message    =   text;
                })
                .catch(function (e)
                {
                    self.log( e );
                });
            }
        },
        computed: {

        },
        watch : {

        },
        created : function()
        {

        }
    });

});
