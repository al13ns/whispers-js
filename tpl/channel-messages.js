

define( [ 'jquery' , 'vue/vue' ,  'app/firebase' , 'bluebird' ] , function( $ , Vue , fb , Promise ) {

    return Vue.extend({
        data    :   function()
        {
            return {
                message :   '',
            };
        },

        computed: {

        },

        methods:    {

            getSortedMessages   :   function ()
            {

                let tmp = this.$sortObjectByKeys( this.$parent.channelMessages( this.$route.params.channelID ).toJSON() );
                // console.log( "UNSRTD" , tmp);
                tmp = Map.fromObject( tmp );
                tmp = tmp.reverse();
                // console.log( "SRTD" , tmp);

                return tmp.toJSON();

                // return this.$sortObjectByKeys( this.$parent.channelMessages( this.$route.params.channelID ).toJSON() );
                // return self.$sortObjectByKeys( self.$store.state.chat.messages.get( self.$route.params.channelID ).toJSON() );
                // return this.$store.state.chat.messages.get( this.$route.params.channelID ).reverse().toJSON();
            },

            getSortedUsers  :   function ()
            {
                let tmp = this.$sortObjectByKeys( this.$parent.channelUsers( this.$route.params.channelID ).toJSON() );

                return tmp;
            },

            sendMessage :   function ()
            {
                //prepare "to" recepient array from channel info
                //encrypt & sign message text

                let channel =   this.$route.params.channelID;
                let text    =   this.message;
                let self    =   this;

                self.$parent.submitMessage({ channel , text }).then(() =>
                {
                    self.$parent.log( 'message sent to channel ' + channel );
                });

                self.message = '';
            },

            leaveChannel    :   function ()
            {
                const self = this;

                self.$parent.leaveChannel( self.$route.params.channelID )
                .then(( channel ) =>
                {
                    self.$parent.log( 'left channel ' + channel );

                    self.$router.push({ name : 'channel-list' });
                });
            },
        },

        created : function () {

            this.$store.subscribe((mutation, state) => {

                if( 'addMessage' === mutation.type || 'setChannels' === mutation.type )
                {
                    // this.messages = this.getSortedMessages();
                    this.$forceUpdate();
                    this.$parent.$forceUpdate();
                }
            })

        },

        beforeRouteUpdate    :   function(to, from, next)
        {
            let channel =   to.params.channelID;

            if( this.$parent.isChannelJoined( channel ) )
            {
                next();
                return;
            }

            next( new Error( "not joined" ) );
        }
    });

});
