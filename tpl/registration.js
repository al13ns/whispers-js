
define( [ 'jquery' , 'vue/vue' , 'app/store' , 'whispers' , 'bluebird' ] , function( $ , Vue , store , Whispers , Promise ) {

    return Vue.extend({
        data    :   function()
        {
            return {
                user        :   {
                    name    :   '' ,
                    email   :   '' ,
                } ,
                password    :   '' ,
                privateKey  :   '' ,
                publicKey   :   '' ,
                savePrivKey :   false ,
            };
        },

        methods: {
            log : function (message)
            {
                this.$parent.log( message );
            },

            register    :   function()
            {
                this.verifyKeys();

                let self        =   this;
                let user        =   new Whispers.User( self.user.name , self.password , self.publicKey , self.privateKey );

                self.log( "checking if username available" );

                store.getters.whispers.userExists( user.userID )
                .then( function( result )
                {
                    console.log(result);
                    self.log( 'user exists' );
                } )
                .catch(function(error)
                {
                    console.log(error);

                    if( -1 !== error.toString().indexOf( 'Reference.child' ) )
                    {
                        self.log( 'invalid username' );
                        throw new Error( 'invalid username' );
                    }

                    self.log( "creating user" );

                    store.getters.whispers.createUser( user , self.savePrivKey ).then(function( user )
                    {
                        self.log( "user '" + user.userID + "' created" );
                    });
                });
            },

            unregister  :   function()
            {
                this.verifyKeys();

                let self        =   this;
                let user        =   new Whispers.User( self.user.name , self.password , self.publicKey , self.privateKey );

                self.log( "checking if username exists" );

                store.getters.whispers.userEquals( user , store.getters.crypt )
                .then( function( result )
                {
                    self.log( 'user exists' );

                    store.getters.whispers.removeUser( user.userID ).then(function ()
                    {
                        self.log( "user '" + user.userID + "' removed" );
                    });
                } )
                .catch(function(error)
                {
                    self.log( "user does not exist" );
                });
            },

            userIDfromKey   :   function()
            {
                this.user.name  =   store.getters.crypt.getUserIdFromKey( this.verifyKeys() );
            },

            verifyKeys : function ()
            {
                let pub     =   this.publicKey;
                let priv    =   this.privateKey;
                let pass    =   this.password;

                this.log( "verifying keys" );

                try
                {
                    let key = store.getters.crypt.importPrivAndPubKey( pub , priv , pass );

                    this.log( 'key pair verification succeeded' , "success" );

                    return key;
                }
                catch( err )
                {
                    this.log( 'key pair verification failed' , "danger" );
                    this.log( err , 'danger' );
                    throw err;
                }
            },

            clickPrivKeyFile: function()
            {
                $( this.$refs.privKeyFile ).click();
            },
            loadPrivateKey : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.privKeyFile.files[0] )
                .then( function( content )
                {
                    self.privateKey =   content;
                });
            },

            clickPubKeyFile: function()
            {
                $( this.$refs.pubKeyFile ).click();
            },
            loadPublicKey : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.pubKeyFile.files[0] )
                .then( function( content )
                {
                    self.publicKey =   content;
                });
            },

            clickMessageFile: function()
            {
                $( this.$refs.messageFile ).click();
            },
            loadMessage : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.messageFile.files[0] )
                .then( function( content )
                {
                    self.message =   content;
                });
        },
        }
    });

});
