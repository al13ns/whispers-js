

define( [ 'jquery' , 'vue/vue' , 'bluebird' ] , function( $ , Vue , Promise ) {

    return Vue.extend({
        data    :   function()
        {
            return {
                channel :   ""
            };
        },

        methods :   {
            createChannel   :   function ()
            {
                const self      =   this;
                const channel   =   this.channel;

                if( !channel )
                {
                    self.$parent.log( 'invalid channel name' );
                    return;
                }

                self.$parent.createChannel( self.channel )
                .then( (channel) =>
                {
                    self.$parent.log( 'created channel ' + channel );

                    return self.$parent.joinChannel( channel );
                })
                .catch(( error ) =>
                {
                    self.$parent.log( 'channel already exists ' + channel );
                })
                .finally(() =>
                {
                    self.enterChannel( channel );
                });
            },

            enterChannel    :   function ( channel )
            {
                this.$router.push({ 'name' : 'channel-messages' , 'params' : { 'channelID' : channel } });
            },
        },

        created :   function ()
        {

        }
    });

});
