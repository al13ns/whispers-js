

define( [ 'jquery' , 'vue/vue' , 'vue/vue-resource' , 'app/store' , 'bluebird' ] , function( $ , Vue , VueResource , store , Promise ) {

    Vue.use( VueResource );

    return Vue.extend({
        data    :   function()
        {
            return {
                password    :   '' ,
                message     :   '' ,
                privateKey  :   '' ,
                publicKey   :   '' ,
                action      :   {
                    encDec  :   false ,
                    sigVer  :   false ,
                },
                partners    :   [] ,
            };
        },
        methods :   {
            log : function (message)
            {
                this.$parent.$parent.log( message );
            },
            addPartner  :   function()
            {
                this.partners.push({
                    publicKey   :   '' ,
                    username    :   '' ,
                });
            },
            box : function ()
            {
                let self    =   this;
                let crypt   =   store.getters.crypt;
                let pub     =   this.publicKey;
                let priv    =   this.privateKey;
                let pass    =   this.password;
                let text    =   this.message;
                let keys    =   this.importPartnerKeys();

                // keys.push( crypt.importPrivKey( priv , pass ) );
                // keys.push( crypt.importPubKey( pub ) );

                if( self.action.encDec && self.action.sigVer )
                {
                    keys.push( crypt.importPrivKey( priv , pass ) );

                    crypt.signAndEncrypt(
                        keys ,
                        crypt.importPrivKey( priv , pass ) ,
                        text )
                    .then( function( cipher )
                    {
                        self.message    =   cipher;
                    })
                    .catch(function (e)
                    {
                        self.log( e );
                    });
                }
                else if( self.action.encDec )
                {
                    try
                    {
                        keys.push( crypt.importPrivKey( priv , pass ) );
                    }
                    catch(e)
                    {
                        keys.push( crypt.importPubKey( pub ) );
                    }

                    crypt.encrypt(
                        keys ,
                        text )
                    .then( function( cipher )
                    {
                        self.message    =   cipher;
                    })
                    .catch(function (e)
                    {
                        self.log( e );
                    });
                }
                else if( self.action.sigVer )
                {
                    crypt.sign(
                        crypt.importPrivKey( priv , pass ),
                        text )
                    .then( function( signedtext )
                    {
                        self.message    =   signedtext;
                    })
                    .catch(function (e)
                    {
                        self.log( e );
                    });
                }
            },
            unbox : function ()
            {
                let self    =   this;
                let crypt   =   store.getters.crypt;
                let pub     =   this.publicKey;
                let priv    =   this.privateKey;
                let pass    =   this.password;
                let text    =   this.message;
                let keys    =   this.importPartnerKeys();

                if( self.action.encDec && self.action.sigVer )
                {
                    keys.push( crypt.importPrivKey( priv , pass ) );

                    crypt.decryptAndVerify(
                        crypt.importPrivKey( priv , pass ) ,
                        keys ,
                        text )
                    .then( function( plaintext )
                    {
                        self.message    =   plaintext;
                    })
                    .catch( function( error )
                    {
                        self.log( error , 'danger' );
                        console.log( 'ERROR' , error );
                    });
                }
                else if( self.action.encDec )
                {
                    crypt.decrypt(
                        crypt.importPrivKey( priv , pass ) ,
                        text )
                    .then( function( plaintext )
                    {
                        self.message    =   plaintext;
                    })
                    .catch( function( error )
                    {
                        self.log( error , 'danger' );
                        console.log( 'ERROR' , error );
                    });
                }
                else if( self.action.sigVer )
                {
                    try
                    {
                        keys.push( crypt.importPrivKey( priv , pass ) );
                    }
                    catch(e)
                    {
                        keys.push( crypt.importPubKey( pub ) );
                    }

                    crypt.verify(
                        keys ,
                        text )
                    .then( function( plaintext )
                    {
                        console.log( plaintext );
                        self.message    =   plaintext;
                    } )
                    .catch( function( error )
                    {
                        self.log( error , 'danger' );
                        console.log( 'ERROR' , error );
                    });
                }
            },
            verifyKeys : function ()
            {
                let pub     =   this.publicKey;
                let priv    =   this.privateKey;
                let pass    =   this.password;

                try
                {
                    let key = store.getters.crypt.importPrivAndPubKey( pub , priv , pass );

                    if( key.isPrivate() )
                    {
                        this.log( 'key pair verification succeeded' , "success" );
                    }
                    else
                    {
                        throw new Error( "key is not private" );
                    }
                }
                catch( err )
                {
                    this.log( 'key pair verification failed' , "danger" );
                    this.log( err , 'danger' );
                    throw err;
                }
            },

            clickPrivKeyFile: function()
            {
                $( this.$refs.privKeyFile ).click();
            },
            loadPrivateKey : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.privKeyFile.files[0] )
                .then( function( content )
                {
                    self.privateKey =   content;
                });
            },

            clickPubKeyFile: function()
            {
                $( this.$refs.pubKeyFile ).click();
            },
            loadPublicKey : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.pubKeyFile.files[0] )
                .then( function( content )
                {
                    self.publicKey =   content;
                });
            },

            clickMessageFile: function()
            {
                $( this.$refs.messageFile ).click();
            },
            loadMessage : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.messageFile.files[0] )
                .then( function( content )
                {
                    self.message =   content;
                });
            },

            clickPartnerKeyFile: function( key )
            {
                $( this.$refs.partnerKey[key] ).click();
            },
            loadPartnerKey : function ( key )
            {
                let self = this;

                self.$loadTextFile( self.$refs.partnerKey[key].files[0] )
                .then( function( content )
                {
                    self.partners[key].publicKey =   content;

                    return content;
                })
                .then(( content ) =>
                {
                    self.readPartnerUsername( key );
                });
            },
            readPartnerUsername :   function ( key )
            {
                try
                {
                    this.partners[key].username =   store.getters.crypt.getUserIdFromKey( store.getters.crypt.importPubKey( this.partners[key].publicKey ) );
                }
                catch(e)
                {
                    console.log( e );

                    if( -1 !== e.message.indexOf( 'getPrimaryUser' ) || -1 !== e.message.indexOf( 'key is undefined' )  )
                    {
                        this.log( 'invalid key' );
                    }
                    else
                    {
                        this.log( e );
                    }
                }
            },
            removePartner    :   function( key )
            {
                this.partners.splice( key , 1 );
            },

            importPartnerKeys :   function ()
            {
                return this.partners.map((p) => store.getters.crypt.importPubKey(p.publicKey) );
            }
        },
        computed: {

        },
        watch : {

        },
        created : function()
        {
            // this.addPartner();
        }
    });

});
