

define( [ 'jquery' , 'vue/vue' , 'vue/vue-resource' , 'app/store' ] , function( $ , Vue , VueResource , store ) {

    Vue.use( VueResource );

    return Vue.extend({
        data    :   function()
        {
            return {
                user        :   {
                    name    :   '' ,
                    email   :   '' ,
                } ,
                password    :   '' ,
                privateKey  :   '' ,
                publicKey   :   '' ,
            };
        },
        methods :   {
            log : function (message)
            {
                this.$parent.$parent.log( message );
            },
            generateKeys : function ()
            {
                let crypt   =   store.getters.crypt;
                let user    =   this.user.name;
                let pass    =   this.password;
                let self    =   this;

                self.log( "generating key pair" );
                self.log( "this may take a while" );
                self.log( "be patient" );

                crypt.generateKeyPair( user , pass ).then(
                function( keyPair )
                {
                    self.log( "sucessfully generated key pair" , 'success' );
                    self.log( "save keys to safe location" , 'success' );

                    self.privateKey =   keyPair.privateKeyArmored;
                    self.publicKey  =   keyPair.publicKeyArmored;
                },
                function( err )
                {
                    console.log( err );
                    self.log( err , 'danger' );
                });
            },
        },
        computed: {

        },
        watch : {

        },
        created : function()
        {

        }
    });

});
