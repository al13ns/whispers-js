

define( [ 'jquery' , 'vue/vue' ] , function( $ , Vue ) {

    return Vue.extend({
        data    :   function()
        {
            return {
            };
        },

        methods :   {
            joinChannel :   function ( channel )
            {
                let self = this;

                self.$parent.joinChannel( channel )
                .then( function ()
                {
                    self.$parent.log( 'joined channel ' + channel );

                    self.enterChannel( channel );
                } );
            },

            enterChannel    :   function ( channel )
            {
                this.$router.push({ 'name' : 'channel-messages' , 'params' : { 'channelID' : channel } });
            },

            leaveChannel    :   function ( channel )
            {
                this.$parent.leaveChannel( channel );
                this.$parent.log( 'left channel ' + channel );
            },
        },

        created :   function ()
        {
            // this.$store.dispatch( 'subscribeChannels' );
        }
    });

});
