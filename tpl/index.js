
define( [ 'jquery' , 'vue/vue' , 'bluebird' , 'underscore' ] , function( $ , Vue , Promise , _ ) {

    return Vue.extend({
        data    :   function()
        {
            return {
                links   :   {
                    'OpenPGP.js'    :   'http://openpgpjs.org/' ,
                    'Firebase'      :   'https://firebase.google.com/' ,
                    'bluebird'      :   'http://bluebirdjs.com' ,
                    'Semantic UI'   :   'https://semantic-ui.com/' ,
                    'Require JS'    :   'http://requirejs.org/' ,
                    'Vue.js'        :   'https://vuejs.org/' ,
                    'Vuex'          :   'https://vuex.vuejs.org/' ,
                    'Vue Router'    :   'https://router.vuejs.org/' ,
                    'Vue-i18n'      :   'http://kazupon.github.io/vue-i18n/en/' ,
                    'Moment.js'     :   'https://momentjs.com' ,
                    'jQuery'        :   'http://jquery.com/' ,
                    'underscorejs'  :   'http://underscorejs.org/' ,
                    'Web Workers'   :   'https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API' ,
                    'window.crypto' :   'https://developer.mozilla.org/en-US/docs/Web/API/Window/crypto' ,
                }
            };
        },

        created :   function ()
        {
            // Promise.method(); //wraps func
            // Promise.try(); //wraps & calls func

            //store messages encrypted
            //run throttle'd serial decryption

            return;

            let i = 0;
            let something = _.throttle( function()
            {
                console.log( "pass " , ++i );
            } , 1000);

            while(false)
            {
                something();
            }

            Promise.try( () =>
            {
                 if( false )
                 {
                     throw new Error( "some error" );
                 }

                 return 666;
            })
            .then(( data ) =>
            {
                console.log( "your data, sir: " , data );
            })
            .catch(( err ) =>
            {
                console.log( "there was an error: " , err );
            });

            let arr = [ 'first' , 'second' , 'third' , 'fourth' , 'fifth' ];

            Promise.map( arr , function (item)
            {
                return item.toUpperCase();
            } ,
            { concurrency : 1 })
            .then(( data ) =>
            {
                console.log( "data here: " , data );
            });

            let whispers    =   this.$store.getters.whispers;

            whispers.userExists( 'olaf' ).then( function( result )
            {
                console.log(result);
            } )
            .catch(function(error)
            {
                console.log(error);
            });

            whispers.createUser( { userID : 'XXXXX' , privKey : 'fsds' , pubKey : 'jkjl' , password : 'kokor' } , true )
            .then( function( result )
            {
                console.log(result);
            } )
            .catch(function(error)
            {
                console.log(error);
            });

        }
    });

});
