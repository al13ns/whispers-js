
define( [ 'jquery' , 'vue/vue' , 'app/store' , 'whispers' ] , function( $ , Vue , store , Whispers ) {

    return Vue.extend({
        data    :   function()
        {
            return {
                user        :   {
                    name    :   '' ,
                    email   :   '' ,
                } ,
                password    :   '' ,
                privateKey  :   '' ,
            };
        },

        methods: {
            log : function (message)
            {
                this.$parent.log( message );
            },

            login   :   function ()
            {
                let auth        =   new Whispers.User( this.user.name , this.password , null , this.privateKey );
                const self = this;

                store.getters.whispers.userLogin( auth , store.getters.crypt ).then(function(user)
                {
                    delete user.password;
                    store.commit( 'setUser' , user );
                    console.log( user );
                    console.log( store );

                    self.log( 'logged in as ' + user.toString() );

                    store.dispatch( 'subscribeChannels' );
                    store.dispatch( 'subscribeMessages' );

                    if( self.$route.query && self.$route.query.redirect )
                    {
                        self.$router.push({ 'path' : self.$route.query.redirect });
                    }
                })
                .catch(( err ) =>
                {
                    if( -1 !== err.toString().indexOf( 'argument was an invalid path' ) )
                    {
                        self.log( "invalid credentials" );
                    }
                    else
                    {
                        self.log( err );
                    }
                });
            },

            logout  :   function ()
            {
                if( true )
                {
                    window.location.assign( '' );
                    // window.location.assign( this.$router.resolve({ 'name' : 'index' }).href );
                    return;
                }

                for( const [ channel , ch ] of store.getters.joinedChannels )
                {
                    store.getters.whispers.db
                    .child( 'messages' )
                    .child( channel )
                    .off();
                }

                store.getters.whispers.db
                .child( 'channels' )
                .off();

                store.commit( 'setUser' , null );
                store.commit( 'setChannels' , {} );

                this.log( 'logged out' );
            },

            clickPrivKeyFile: function()
            {
                $( this.$refs.privKeyFile ).click();
            },
            loadPrivateKey : function ()
            {
                let self = this;

                self.$loadTextFile( self.$refs.privKeyFile.files[0] )
                .then( function( content )
                {
                    self.privateKey =   content;
                });
            },
        }
    });

});
