
// 'use strict';

require.config({
    baseUrl: "assets/js/",
    paths: {
        // "some": "some/v1.0"
        'smileys'           :   '../../assets/smileys/smileys' ,
        'emoticons'         :   '../../assets/emoticons/jquery.cssemoticons' ,
        'app/store'         :   '../../app/store' ,
        'app/translations'  :   '../../app/translations' ,
        'app/firebase'      :   '../../app/firebase' ,
        'openpgp'           :   '../../assets/js/openpgp/openpgp.min' ,
        'bluebird'          :   '../../assets/js/bluebird.min' ,
        'underscore'        :   '../../assets/js/underscore-min' ,
    },

    shim: {
        'smileys': {
            deps: ['jquery']
        },
        'emoticons': {
            deps: ['jquery']
        },
    }
});

requirejs([
    'jquery' , 'app/firebase' , 'smileys' ,
    'utils' , 'bluebird' ,
    'vue/vue' , 'vue/vuex' , 'vue/vue-router' ,
    'vue/vue-resource' , 'semantic-ui/boot' ,
    'vue/loader' , 'vue/app-mixin' , 'vue/cookies-mixin' , 'vue/moment-mixin' ,
    'app/store' , 'app/translations'
    ],
    function(
    jQuery , fb , emoticons ,
    utils , Promise ,
    Vue , Vuex , VueRouter ,
    VueResource , SemanticUI ,
    VueLoader , VueAppMixin , VueCookiesMixin , VueMoment ,
    store , i18n ) {
//This function is called when scripts/helper/util.js is loaded.
//If util.js calls define(), then this function is not fired until
//util's dependencies have loaded, and the util argument will hold
//the module value for "helper/util".

        Promise.config({
            longStackTraces: true,
            warnings: true // note, run node with --trace-warnings to see full stack traces for warnings
        });

        Vue.use( VueLoader , { 'htmlUri' : 'tpl/' , 'jsUri' : 'tpl/' } );
        Vue.use( VueRouter );
        Vue.use( Vuex );
        Vue.use( VueResource );
        Vue.mixin( VueMoment );
        Vue.mixin({
            methods : {
                ...utils
            }
        });
        VueRouter.createRoute =   function( name , path , component , requiresAuth = false )
        {
            path        =   path || '/' + name;
            component   =   component || Vue.getComponent( name );

            return {
                'path'        :   path ,
                'name'        :   name ,
                'component'   :   component ,
                'meta'        :   { 'requiresAuth' : requiresAuth }
            };
        };
        VueRouter.createRoutes = function( names )
        {
            let routes  =   [];

            for( let i in names )
            {
                routes.push( this.createRoute( names[i] ) );
            }

            return routes;
        };

        let $ = jQuery;

        (function()
        {
            let $tpls   =   $('#templates');
            let cmpnts  =   [
                'index' , 'tools' , 'tools-sym' , 'tools-asym' , 'tools-asym-keygen' ,
                'registration' , 'login' , 'channel-list' , 'channel-create' , 'channel-messages'
            ];

            for( let i in cmpnts )
            {
                let $xtpl =  $( '<script type="x/template" id="cmpnt-'+ cmpnts[i] +'" data-name="'+ cmpnts[i] +'">' );

                $tpls.append( $xtpl);
            }

        })();

        // Vue.loadComponents([ '#cmpnt-index' , '#cmpnt-chat' ]).then(function(values) {
        Vue.loadComponents( '#templates' ).then(function(values) {

            Vue.component('navigation',
            function( resolve , reject )
            {
                // resolve(cmp.extend({
                //     template: ele
                // }));

                resolve({
                    data    :   function()
                    {
                        return {};
                    },
                    mounted: function () {
                        let self = this;
                        self.$nextTick(function () {
                            console.log(self.$parent);
                            console.log(self.$parent.$i18n);
                        })
                    }
                })
            });

            let routes  =   [
                Object.assign( VueRouter.createRoute( 'tools' )  ,
                { children: [
                    VueRouter.createRoute( 'tools-sym' , '/tools/sym' ) ,
                    VueRouter.createRoute( 'tools-asym' , '/tools/asym' ) ,
                    VueRouter.createRoute( 'tools-asym-keygen' , '/tools/asym-keygen' )
                ] } ) ,
                VueRouter.createRoute( 'index' ) ,
                VueRouter.createRoute( 'registration' , '/register' ) ,
                VueRouter.createRoute( 'login' , '/login' ) ,
                VueRouter.createRoute( 'channel-list' , '/channels' , null , true ) ,
                VueRouter.createRoute( 'channel-create' , '/channels/new' , null , true ) ,
                VueRouter.createRoute( 'channel-messages' , '/channels/:channelID' , null , true ) ,
                { path: '/', redirect: { name: 'index' }}
            ];

            const router    =   new VueRouter({
                linkActiveClass : 'active' ,
                routes : routes,
                // routes : VueRouter.createRoutes([ 'index' , 'chat' , 'todolist' , 'yesno' , 'weather' ]) ,
            });

            const app       =   new Vue({
                el: '#app',
                router: router,
                store: store ,
                i18n: i18n ,
                mixins: [ VueAppMixin , VueCookiesMixin ] ,
                methods: {
                    ...Vuex.mapMutations([
                        'setLocale'
                    ]),
                    ...Vuex.mapActions([
                        'submitMessage' ,
                        'joinChannel' ,
                        'leaveChannel' ,
                        'createChannel'
                    ]),
                    log : function ( message )
                    {
                        $( '<p></p>' ).text( message ).appendTo( $( '#log' ) );
                    },
                    clearLog : function ()
                    {
                        $( '#log' ).empty();
                    },
                    setLocale : function( locale )
                    {
                        if( -1 === this.getAvailableLocales().indexOf( locale ) )
                        {
                            throw new Error( "locale not alowed" );
                            return;
                        }

                        this.$momentSetLocale( locale );
                        this.$i18n.locale   =   locale;
                    },
                    getAvailableLocales :   function ()
                    {
                        let locales =   [];

                        for( let i in this.$i18n.messages )
                        {
                            locales.push(i);
                        }

                        return locales;
                    },
                },
                computed: {
                    ...Vuex.mapGetters([
                        'user' ,
                        'joinedChannels' ,
                        'channelMessages' ,
                        'channelUsers' ,
                        'isChannelJoined'
                    ]),
                }
                // template: '<router-view></router-view>'
            });

            SemanticUI.init();
            // $('.emoticons').emoticonize();
            $('.emoticons').smilify();

            // store.dispatch( 'subscribeMessages' );
            // app.setCookie( 'bla' , 'blabla' );
            // app.setLocale( 'cs' );
            // app.$store.dispatch( 'setLocale' , 'cs' );
            // console.log( app.$store.state );
            // console.log( app.$store.state.crypt.symEncrypt( '123456' , 'nazdárek' ) );

            return values;
        })
        .then(function(values)
        {

        });

    });
