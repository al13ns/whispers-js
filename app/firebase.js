
define( [ 'firebase/firebase' ] , function( fb ) {

    const config = {
        apiKey: "AIzaSyCZpj1X8XGSB1tCXSFDAxgV33wWD9C4IVA",
        authDomain: "whispers-36e9c.firebaseapp.com",
        databaseURL: "https://whispers-36e9c.firebaseio.com",
        projectId: "whispers-36e9c",
        storageBucket: "whispers-36e9c.appspot.com",
        messagingSenderId: "423771637044"
    };
    firebase.initializeApp(config);

    return {
        'db'        :   firebase.database().ref( 'whispers' ) ,
        'sb'        :   firebase.database().ref() ,
        'firebase'  :   firebase
    };
});

