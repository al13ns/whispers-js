
define( [ 'vue/vue' , 'vue/vue-i18n' ] , function( Vue , VueI18n ) {

    Vue.use( VueI18n );

    const messages = {
        'en'    :   {
            'layout'    :   {
                'title' :   'Whispers' ,
                'navigation' : {
                    'index' :   'WTF' ,
                    'tools' :   'Crypto tools' ,
                    'registration'  :   'Sign up' ,
                    'login'     :   'Sign in' ,
                    'logout'    :   'Sign out' ,
                    'channels'  :   'Channels'
                },
                'clearLog'  :   'Clear Log' ,
            },
            'tools' :   {
                'sym'           :   'Symmetric' ,
                'asym'          :   'Asymmetric' ,
                'asymKeygen'    :   'Asymmetric keygen' ,
                'generatePass'  :   'Generate password' ,
                'generateKeys'  :   'Generate keys' ,
                'encrypt'  :   'Encrypt' ,
                'decrypt'  :   'Decrypt' ,
                'sign'     :    'Sign' ,
                'verify'    :   'Verify' ,
                'box'       :   'Box' ,
                'unbox'     :   'Unbox' ,
                'partners' : 'Partners' ,
                'addPartner'    :   'Add partner' ,
                'removePartner' :   'Remove partner' ,
            },
            'common'    :   {
                'name'  :   'Username' ,
                'email' :   'Email' ,
                'privKey'   :   'Private key' ,
                'pubKey'    :   'Public key' ,
                'privKeyPass'   :   'Private key password' ,
                'verifyKeys'    :   'Verify keys' ,
                'options'   :   'Options' ,
                'password'  :   'Password' ,
                'message'   :   'Message' ,
                'or'        :   'or' ,
            },
            'registration'    :   {
                'savePrivKey'   :   'Save private key' ,
                'nameFromKey'   :   'Name from key' ,
                'register'      :   'Register' ,
                'unregister'    :   'Unregister' ,
            },
            'channel'   :   {
                'list'  :   'Channel list' ,
                'create':   'Create channel' ,
                'join'  :   'Join channel' ,
                'enter'  :   'Enter channel' ,
                'leave'  :   'Leave channel' ,
                'name'  :   'Channel name' ,
                'users'  :   'Channel users' ,
                'messages'  :   'Channel messages' ,
                'message'   :   {
                    'text'   :   'Message text' ,
                    'send'      :   'Send Message' ,
                },
            },
        },
        'cs'    :   {
            'layout'    :   {
                'title' :   'Whispers' ,
                'navigation' : {
                    'index' :   'WTF' ,
                    'tools' :   'Kryptonástroje' ,
                    'registration'  :   'Registrovat se' ,
                    'login' :   'Přihlásit se' ,
                    'logout' :   'Odhlásit se' ,
                    'channels' :   'Místnosti' ,
                },
                'clearLog'  :   'Smazat Log' ,
            },
            'tools' :   {
                'sym'           :   'Symetrické' ,
                'asym'          :   'Asymetrické' ,
                'asymKeygen'    :   'Generátor asymetrických klíčů' ,
                'generatePass'  :   'Generovat heslo' ,
                'generateKeys'  :   'Generovat klíče' ,
                'encrypt'  :   'Zašifrovat' ,
                'decrypt'  :   'Dešifrovat' ,
                'sign'      :   'Podepsat' ,
                'verify'    :   'Ověřit' ,
                'box'       :   'Zabalit' ,
                'unbox'     :   'Rozbalit' ,
                'partners' : 'Partneři' ,
                'addPartner'    :   'Přidat partnera' ,
                'removePartner' :   'Odstranit partnera' ,
            },
            'common'    :   {
                'name'  :   'Uživatelské jméno' ,
                'email' :   'Email' ,
                'privKey'   :   'Soukromý klíč' ,
                'pubKey'    :   'Veřejný klíč' ,
                'privKeyPass'   :   'Heslo soukromého klíče' ,
                'verifyKeys'    :   'Ověřit klíče' ,
                'options'   :   'Možnosti' ,
                'password'  :   'Heslo' ,
                'message'   :   'Zpráva' ,
                'or'        :   'či' ,
            },
            'registration'    :   {
                'savePrivKey'   :   'Uložit soukromý klíč' ,
                'nameFromKey'   :   'Jméno z klíče' ,
                'register'      :   'Zaregistrovat se' ,
                'unregister'    :   'Odregistrovat se' ,
            },
            'channel'   :   {
                'list'  :   'Seznam místností' ,
                'create':   'Vytvořit místnost' ,
                'join'  :   'Přihlásit se do místnosti' ,
                'enter'  :   'Vstoupit do místnosti' ,
                'leave'  :   'Opustit místnost' ,
                'name'  :   'Název místnosti' ,
                'users'  :   'Uživatelé v místnosti' ,
                'messages'  :   'Zprávy v místnosti' ,
                'message'   :   {
                    'text'   :   'Text zprávy' ,
                    'send'      :   'Odeslat zprávu' ,
                },
            },
        },
    };

    // Create VueI18n instance with options
    const i18n = new VueI18n({
        locale: 'en', // set locale
        fallbackLocale: 'en' ,
        messages, // set locale messages
    });

    return i18n;
});

