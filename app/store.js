
define( [ 'jquery' , 'vue/vue' , 'vue/vuex' , 'app/firebase' ,
        'utils' , 'crypt' , 'whispers' , 'bluebird' , 'underscore' ] ,
    function( $ , Vue , Vuex , fb , utils , Crypt , Whispers , Promise , _ ) {

    Vue.use( Vuex );

    const decrypt = _.debounce( function( dispatch )
    {
        console.log( "decrypting ");
        dispatch( 'runDecryptor' );
    } , 1000);

    return new Vuex.Store({
        // ...
        strict: true ,
        state: {
            crypt: new Crypt( 'assets/js/openpgp/openpgp.worker.min.js' ).init() ,
            whispers : new Whispers() ,
            chat: {
                msgsToDecrypt   :   new Map() ,
                messages    :   new Map() ,
                channels    :   new Map() ,
                keys        :   new Map() ,
            },
            auth : {
                user        :   null ,
            }
        },
        mutations: {
            setChannels :   ( state , channels )            =>  { state.chat.channels = utils.$createMap( channels ) } ,
            setUser     :   ( state , user )                =>  { state.auth.user = user } ,
            addKey      :   ( state , { key , userID } )    =>  { state.chat.keys.set( userID , key ) } ,
            addMessage  :   ( state , { message , key , channel } ) =>  {

                if( !state.chat.messages.has( channel ) )
                {
                    state.chat.messages.set( channel , new Map() );
                }

                state.chat.messages.get( channel ).set( key , message );
            },
            addMessageToDecrypt  :   ( state , { message , key , channel } ) =>  {

                if( !state.chat.msgsToDecrypt.has( channel ) )
                {
                    state.chat.msgsToDecrypt.set( channel , new Map() );
                }

                state.chat.msgsToDecrypt.get( channel ).set( key , message );
            },
            removeMessageDecrypt    :   ( state , { key , channel } ) =>
            {
                state.chat.msgsToDecrypt.get( channel ).delete( key );
            }
        },
        actions: {
            runDecryptor    :  ({ state , getters , dispatch , commit }) =>
            {
                return new Promise(function(resolve,reject)
                {
                    return Promise.map( state.chat.msgsToDecrypt , function( ch )
                    {
                        let channel  =   ch[0];

                        return Promise.map( ch[1] , function( m  )
                        {
                            let message =   m[1];

                            return dispatch( 'getPublicKey' , message.from )
                            .then(function( pubKey )
                            {
                                return getters.whispers.decryptMessage( getters.user , pubKey , message.text , getters.crypt );
                            })
                            .then( function( plaintext )
                            {
                                message.text    =   plaintext;

                                commit( 'addMessage' , { message , key : message['.key'] , channel } );
                            })
                            .catch( function(error)
                            {
                                console.log( "decryption error" , error );

                            })
                            .finally(function()
                            {
                                commit( 'removeMessageDecrypt' , { message , key : message['.key'] , channel } );
                            })
                            ;
                        },{ concurrency : 2 });
                    },{ concurrency : 1 });
                });
            },
            subscribeChannels   :   ({ commit , state , getters }) =>
            {
                // getters.whispers.fetchChannels().then( function( channels )
                // {
                //     commit( 'setChannels' , channels );
                // });

                getters.whispers.subscribeChannels( function( channels )
                {
                    commit( 'setChannels' , channels );
                });
            },
            messageListener :  ({ commit , dispatch }) => ( message , key , channel ) =>
            {
                new Promise( function( resolve , reject )
                {
                    message['.key'] =   key;

                    commit( 'addMessageToDecrypt' , { message , key , channel } );

                    resolve( message );
                })
                .then(function()
                {
                    decrypt(dispatch);
                });
            },
            subscribeMessages : ({ commit , state , getters , dispatch }) => {

                dispatch( 'messageListener' ).then( ( listener ) => {
                    getters.whispers.subscribeMessages( listener , getters.user , getters.crypt );
                } );

                // this.someObject = Object.assign({}, this.someObject, { a: 1, b: 2 })
                //     commit( 'setMessages' , Object.assign({}, state.chat.messages ,{ [snap.key] : snap.val() } ) );
            },
            submitMessage : ({ getters , state } , { channel , text } ) => {
                return getters.whispers.submitMessage( getters.user , channel , text , getters.crypt );
            },
            getPublicKey    :   ( { state , getters , commit } , userID ) => {
                return new Promise( function( resolve , reject )
                {
                    if( state.chat.keys.has( userID ) )
                    {
                        resolve( state.chat.keys.get( userID ) );
                    }
                    else
                    {
                        getters.whispers.loadPubKey( userID , getters.crypt )
                        .then( function( key )
                        {
                            commit( 'addKey' , { key , userID : userID } );
                            resolve( key );
                        } );
                    }
                })
            },
            joinChannel :   ({ getters , dispatch } , channel ) => {
                dispatch( 'messageListener' ).then( ( listener ) => {
                    getters.whispers.joinChannel( listener , getters.user , channel );
                } );
            },
            leaveChannel :   ({ getters } , channel ) => {
                //also empty cached messages?
                return getters.whispers.leaveChannel( getters.user , channel );
            },
            createChannel :   ({ getters } , channel ) => {
                return getters.whispers.createChannel( channel );
            },
        },
        getters: {
            crypt    : ( state ) => state.crypt ,
            user     : ( state ) => state.auth.user ,
            whispers : ( state ) => state.whispers ,
            joinedChannels  :   ( state , getters ) =>  {

                let joined  =   new Map();

                state.chat.channels.forEach(function( val , key )
                {
                    if( val.users && val.users[getters.user] )
                    {
                        joined.set( key , val );
                    }
                });

                return joined;
            } ,
            isChannelJoined :   ( state , getters ) => {
                return ( channel ) => {
                    if( state.chat.channels.has( channel ) && getters.user )
                    {
                        if( state.chat.channels.get( channel ).users )
                        {
                            if( state.chat.channels.get( channel ).users[getters.user.userID] )
                            {
                                return true;
                            }
                        }
                    }

                    return false;
                };
            },
            channelMessages : ( state , getters ) => {
                return ( channel ) => {

                    if( !state.chat.messages.has( channel ) )
                    {
                        return new Map();
                    }

                    return state.chat.messages.get( channel ); //Map
                }
            },
            channelUsers : ( state , getters ) => {
                return ( channel ) => {

                    if( !state.chat.channels.has( channel ) )
                    {
                        return new Map();
                    }

                    return Map.fromObject( state.chat.channels.get( channel ).users ); //Map
                }
            },
        },
        modules: {
            // 'translator' :  modTranslator ,
        }
    });

});

/*
                    .orderByChild( 'user' )
                    .equalTo( userId )
                    .limitToLast( 100 )
 */
